#!/usr/bin/python3
import os
import sys
import gitlab
import argparse
import Replicant

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Utility to manually perform a full sync of the mirrored repositories')
parser.add_argument('--config', help='Path to the configuration file to work with', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.config ):
	print("Unable to locate specified configuration file: %s".format(args.config))
	sys.exit(1)

# Now that we know the file exists, initialize our manager
repositoryManager = Replicant.Manager( args.config )

# Connect to the upstream Gitlab server we will be working with
# To do this, we need to get our credentials and hostname first
gitlabHost  = repositoryManager.config.get('Gitlab', 'instance')
gitlabToken = repositoryManager.config.get('Gitlab', 'token')

# Now we can actually connect
gitlabServer = gitlab.Gitlab( gitlabHost, private_token=gitlabToken )

# Start going over all the projects known to the Gitlab server we are connected to
# Each project is assumed to represent a repository in this instance
gitlabProjects = gitlabServer.projects.list(as_list=False)
for gitlabProject in gitlabProjects:
	# Grab the project in question from our local repository manager
	# Trying to register an already registered project is harmless, so we always do a registration
	localProject = repositoryManager.register( gitlabProject.attributes )

	# Make sure the metadata for the local mirror is up to date from Gitlab
	# Registration does not do this for existing projects, hence the need to do it here
	localProject.updateMetadataFromGitlab( gitlabProject.attributes )

	# Note the name of this project to the user
	print("= Registered " + gitlabProject.path_with_namespace)

# Now that everything is up to date, it's time to clone 'em all!
for hash, repository in repositoryManager.knownRepositories.items():
	# Sync time!
	print("== Syncing: " + repository.path)
	repository.refreshMirror()

# TODO: Local symlink tree update (probably best to do repo-by-repo with a cleanup pass at the end to get rid of old names)
