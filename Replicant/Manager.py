import os
import json
import pygit2
import hashlib
import configparser
from .Repository import Repository

# Central class for managing the repository replica
class Manager:

	# Perform initial setup and get ready to go
	def __init__(self, configFile):
		# Make sure our configuration file is readable and exists
		if not os.path.exists( configFile ):
			raise Exception("Configuration file not found for repository specified")

		# Load the configuration file
		self.config = configparser.ConfigParser( interpolation=configparser.ExtendedInterpolation() )
		self.config.read( configFile, encoding='utf-8' )

		# Determine the paths to where we will be storing everything
		# First, the raw repository storage...
		self.repositoriesPrefix = self.config.get('Storage', 'repositories')
		# Next up, the exported symlink tree (for git-daemon, CGit and human use)
		self.exportedTreePrefix = self.config.get('Storage', 'exported-tree')

		# Now make sure those locations exist
		os.makedirs( self.repositoriesPrefix, exist_ok=True )
		os.makedirs( self.exportedTreePrefix, exist_ok=True )

		# Prepare our data structures to hold information about the repositories
		self.knownRepositories = {}
		self.repositoryPathMap = {}

		# Load all details about the locally held repositories
		self.__loadRepositories( directory=self.repositoriesPrefix )

		# Load up the SSH key we will be using for authenticating to the server when fetching repositories
		# First find where it is stored
		publicKeyPath  = self.config.get('Gitlab', 'publicKeyPath')
		privateKeyPath = self.config.get('Gitlab', 'privateKeyPath')
		# Now load it up
		self.sshKeyPair = pygit2.Keypair('git', publicKeyPath, privateKeyPath, '')

	# Load repositories stored within the given directory
	# It is assumed that these were originally created by Replicant or have the necessary metadata stored within them
	def __loadRepositories(self, directory):
		# Start walking over the contents of this directory
		for entry in os.scandir(directory):

			# We're not interested if this isn't a directory, in that case just ignore it
			# These shouldn't even be here
			if not entry.is_dir(follow_symlinks=False):
				continue

			# Next check to see if it's a folder containing more repositories - if so, load it up
			# Replicant always follows the structure 6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b so we check for a length of two here
			# This is a bit of an assumption, but unless something else has modified the tree it should always be true
			if len(entry.name) == 2:
				self.__loadRepositories( entry.path )
				continue

			# In theory we should now only be left with repositories, but we better do some checks first just to be sure
			# Git repositories managed by Replicant will always have a 'replicant.json' file, as well as the normal 'HEAD' file
			# We therefore use their existence as a safety check before proceeding any further...
			headPath = os.path.join( entry.path, 'HEAD' )
			jsonPath = os.path.join( entry.path, 'replicant.json' )
			if not os.path.exists( headPath ) and not os.path.exists( jsonPath ):
				continue

			# Now we know we have a repository we can load it up :)
			repository = Repository( manager=self, storage=entry.path )
			# Register it with our list of known repositories
			self.knownRepositories[ repository.hash ] = repository
			# If it has one, also register it with the path map
			# If the repository was deleted then it will not have a path
			if repository.path is not None and not repository.deleted:
				self.repositoryPathMap[ repository.path ] = repository

	# Determines the uniquely identifying hash for a repository for this replica
	def hashForRepository( self, id ):
		# Generate a SHA-256 hash for this repository ID
		hasher = hashlib.sha256()
		hasher.update( str(id).encode('utf-8') )
		return hasher.hexdigest()

	# Retrieves a given repository using it's identification number
	def retrieve(self, id):
		# Determine the hash for this repository
		hash = self.hashForRepository( id )

		# Check to see if we have it, and if so, return it
		if hash in self.knownRepositories:
			return self.knownRepositories[hash]

		# Otherwise we have no idea
		return None

	# Register a currently unknown repository with the manager
	def register(self, metadata):
		# Calculate the hash for the repository we will be registering...
		hash = self.hashForRepository( metadata['id'] )

		# Check to see if the repository is already known to us (to prevent double ups)
		if hash in self.knownRepositories:
			return self.knownRepositories[hash]

		# Now we know it doesn't already exist, we can make it so!
		repository = Repository( manager=self, metadata=metadata )

		# Register it with our list of known repositories
		self.knownRepositories[ repository.hash ] = repository
		# If it has one, also register it with the path map
		# If the repository was deleted then it will not have a path
		if repository.path is not None and not repository.deleted:
			self.repositoryPathMap[ repository.path ] = repository

		# Finally return the newly registered repository
		return repository

