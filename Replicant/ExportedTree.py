import os

class ExportedTree(object):
    def __init__(self, sStorageRoot, sExportedRoot):
        self._sExportedRoot = sExportedRoot  # root of the exported repo tree
        self._sStorageRoot = sStorageRoot    # root of the sharded storage tree
        self._mExportedReposByHash = {}      # map from hash to unrooted export path, e.g., a2f52... -> websites/kate-kde-org

    def _hashToPath(self, sHash):
        assert type(sHash) == str, "Internal Error: expected hash to be a string"
        assert len(sHash) == 64, "Internal Error: expected hash length to be 64 characters"
        return os.path.join(sStorageRoot, sHash[0:2], sHash[2:4], sHash[4:])

    def addRepoLink(self, sRepoPath, sHash):
        sLinkFrom = self._hashToPath(sHash)
        sLinkTo = os.path.join(self._sExportedRoot, sRepoPath)

        # Calculate full paths and then create a symlink. If the symlink already
        # exists, delete it first
        try:
            os.unlink(sLinkTo)
        except:
            pass
        finally:
            os.symlink(sLinkFrom, sLinkTo)

        # Update our internal maps so that we can look up the repo by key later,
        # for example when removing or moving repos
        self._mExportedReposByHash[sHash] = sRepoPath

    def removeRepoLink(self, sHash):
        # Ensure that we're tracking the repo
        sExportedPath = self._mExportedReposByHash.get(sHash)
        assert sExportedPath, "Inconsistency: removal requested for non existent repository id"

        # Kill the repository. Ignore errors, since we don't really care if the
        # actual path on disk doesn't exist
        sFullExportedPath = os.path.join(self._sExportedRoot, sExportedPath)
        try:
            os.unlink(sFullExportedPath)
        except:
            # We don't care about errors
            pass
        finally:
            # Kill the entry in our internal map of tracked repos
            del self._mExportedReposByHash[sHash]

    def changeRepoLink(self, sNewRepoPath, sHash):
        # Changing a repo's link path should simply be a matter of removing the old
        # link and adding the new one
        self.removeRepoLink(sHash)
        self.addRepoLink(sHash, sNewRepoPath)
